#!/bin/bash
set -e

USAGE_STRING="$0 <SubsystemName> <BoardName>"

if [ "$#" -ne 2 ]
then
  echo "Invalid usage!"
  echo "$USAGE_STRING"
fi


SUBSYSTEMNAME_ORIGINAL=$1
SUBSYSTEMNAME_LOWERCASE=${SUBSYSTEMNAME_ORIGINAL,,}
SUBSYSTEMNAME_LOWERCASE_WITHDASHES=${SUBSYSTEMNAME_LOWERCASE/ /-}
SUBSYSTEMNAME_LOWERCASE=$(echo ${SUBSYSTEMNAME_LOWERCASE} | tr -d ' ')
SUBSYSTEMNAME_UPPERCASE=${SUBSYSTEMNAME_ORIGINAL^^}
SUBSYSTEMNAME_UPPERCASE="${SUBSYSTEMNAME_UPPERCASE/ /_}"

BOARDNAME_ORIGINAL=$2
BOARDNAME_LOWERCASE=${BOARDNAME_ORIGINAL,,}
BOARDNAME_UPPERCASE=${BOARDNAME_ORIGINAL^^}
PLUGIN_REPO_PATH=$(readlink -f $(dirname $BASH_SOURCE))

if [[ "$SUBSYSTEMNAME_ORIGINAL" =~ "[^a-zA-Z0-9 ]" ]]
then
  echo "ERROR: The subsystem name should only contain alphanumeric characters"
  exit 1
fi

if [[ "$BOARDNAME_ORIGINAL" =~ [^a-zA-Z0-9] ]]
then
  echo "ERROR: The board name should only contain alphanumeric characters"
  exit 1
fi


echo "Moving header files from include/example-subsystem to include/${SUBSYSTEMNAME_LOWERCASE_WITHDASHES}"
git mv include/example-subsystem include/${SUBSYSTEMNAME_LOWERCASE_WITHDASHES}
sed -i "s/EXAMPLE_SUBSYSTEM/${SUBSYSTEMNAME_UPPERCASE}/g" $(find ${PLUGIN_REPO_PATH}/include -type f)

echo -e "\nChanging namespace from 'examplesubsystem' to '${SUBSYSTEMNAME_LOWERCASE}'"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" $(find ${PLUGIN_REPO_PATH}/include ${PLUGIN_REPO_PATH}/src -type f)
sed -i "s/examplesubsystem/${SUBSYSTEMNAME_LOWERCASE}/g" $(find ${PLUGIN_REPO_PATH}/include ${PLUGIN_REPO_PATH}/src -type f)
git add $(find ${PLUGIN_REPO_PATH}/include ${PLUGIN_REPO_PATH}/src -type f)

echo -e "\nUpdating herd.yml"
sed -i "s/examplesubsystem/${SUBSYSTEMNAME_LOWERCASE}/g" herd.yml
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" herd.yml
sed -i "s/ExampleBoard/${BOARDNAME_ORIGINAL}/g" herd.yml
git add herd.yml

echo -e "\nUpdating .gitlab-ci.yml"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" .gitlab-ci.yml
sed -i "s/example-subsystem/${SUBSYSTEMNAME_LOWERCASE_WITHDASHES}/g" .gitlab-ci.yml
git add .gitlab-ci.yml

echo -e "\nUpdating CMakeLists.txt"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" CMakeLists.txt
sed -i "s/EXAMPLEBOARD/${BOARDNAME_UPPERCASE}/g" CMakeLists.txt
sed -i "s/examplesubsystem/${SUBSYSTEMNAME_LOWERCASE}/g" CMakeLists.txt
sed -i "s/EXAMPLESUBSYSTEM/${SUBSYSTEMNAME_UPPERCASE}/g" CMakeLists.txt
git add CMakeLists.txt

echo -e "\nUpdating .devcontainer/devcontainer.json"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" .devcontainer/devcontainer.json
sed -i "s/example-subsystem/${SUBSYSTEMNAME_LOWERCASE_WITHDASHES}/g" .devcontainer/devcontainer.json
git add .devcontainer/devcontainer.json

echo -e "\nCommitting the changes"
git commit -m "Subsystem and board names changed to ${SUBSYSTEMNAME_ORIGINAL} and ${BOARDNAME_ORIGINAL}"

echo -e "\nUpdating URL of git remote 'origin'"
git remote set-url origin ssh://git@gitlab.cern.ch:7999/cms-cactus/phase2/software/plugins/subsystems/${SUBSYSTEMNAME_LOWERCASE_WITHDASHES}.git