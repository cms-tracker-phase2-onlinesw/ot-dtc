#ifndef __OT_DTC_SWATCH_COMMANDS_SETTHRESHOLDS_HPP__
#define __OT_DTC_SWATCH_COMMANDS_SETTHRESHOLDS_HPP__

#include "swatch/action/Command.hpp"


namespace otdtc {
namespace swatch {
namespace commands {

// Represents a specific step in the configuration sequence. EXAMPLE: Setting algorithm thresholds
class SetThresholds : public ::swatch::action::Command {
public:
  SetThresholds(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~SetThresholds();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace otdtc


#endif