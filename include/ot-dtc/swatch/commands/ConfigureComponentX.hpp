#ifndef __OT_DTC_SWATCH_COMMANDS_CONFIGURECOMPONENTX_HPP__
#define __OT_DTC_SWATCH_COMMANDS_CONFIGURECOMPONENTX_HPP__

#include "swatch/action/Command.hpp"


namespace otdtc {
namespace swatch {
namespace commands {

// Represents a specific step in the configuration sequence. EXAMPLE: Configuring component X
class ConfigureComponentX : public ::swatch::action::Command {
public:
  ConfigureComponentX(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~ConfigureComponentX();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace otdtc


#endif