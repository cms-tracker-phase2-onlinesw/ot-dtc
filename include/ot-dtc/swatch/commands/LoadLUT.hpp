#ifndef __OT_DTC_SWATCH_COMMANDS_LOADLUT_HPP__
#define __OT_DTC_SWATCH_COMMANDS_LOADLUT_HPP__

#include "swatch/action/Command.hpp"


namespace otdtc {
namespace swatch {
namespace commands {

// Represents a specific step in the configuration sequence. EXAMPLE: Loading a look-up table
class LoadLUT : public ::swatch::action::Command {
public:
  LoadLUT(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~LoadLUT();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace otdtc


#endif