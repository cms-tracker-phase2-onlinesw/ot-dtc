#ifndef __OT_DTC_SWATCH_PROCESSOR_HPP__
#define __OT_DTC_SWATCH_PROCESSOR_HPP__

#include "serenity/swatch/DaughterCard.hpp"


namespace otdtc {
namespace swatch {

// Represents a data-processing FPGA that implements the main function of a board (e.g. the trigger algorithm)
class Processor : public serenity::swatch::DaughterCard {
public:
  Processor(const ::swatch::core::AbstractStub& aStub);
  ~Processor();
};

} // namespace swatch
} // namespace otdtc


#endif