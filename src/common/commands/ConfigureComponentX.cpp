#include "ot-dtc/swatch/commands/ConfigureComponentX.hpp"


#include "ot-dtc/swatch/Processor.hpp"


namespace otdtc {
namespace swatch {
namespace commands {

using namespace ::swatch;


ConfigureComponentX::ConfigureComponentX(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure component X", aActionable, std::string())
{
  // Some parameters that are loaded into the component, or otherwise determine how its configured
  registerParameter<bool>("parameter1", false);
  registerParameter<uint32_t>("parameter2", 0);
  registerParameter<std::string>("mode", "default_mode");
  // FIXME: Modify names, types & multiplicity of parameters as appropriate
}


ConfigureComponentX::~ConfigureComponentX()
{
}


action::Command::State ConfigureComponentX::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from that configure component X
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function


  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if successful overall but something abnormal and worrisome happened along the way
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace otdtc
