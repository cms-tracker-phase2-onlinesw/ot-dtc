#include "ot-dtc/swatch/commands/LoadLUT.hpp"


#include "ot-dtc/swatch/Processor.hpp"


namespace otdtc {
namespace swatch {
namespace commands {

using namespace ::swatch;


LoadLUT::LoadLUT(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Load algorithm look-up table", aActionable, std::string())
{
  // Vector containing the data for the look-up table
  registerParameter<std::vector<uint32_t>>("lutData", {});
  // FIXME: Modify names, types & multiplicity of parameters as appropriate
}


LoadLUT::~LoadLUT()
{
}


action::Command::State LoadLUT::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from that loads the look-up table
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function


  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if successful overall but something abnormal and worrisome happened along the way
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace otdtc
