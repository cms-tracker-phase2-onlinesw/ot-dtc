#include "ot-dtc/swatch/Processor.hpp"

#include "swatch/core/Factory.hpp"

#include "ot-dtc/swatch/Algorithm.hpp"
#include "ot-dtc/swatch/commands/ConfigureComponentX.hpp"
#include "ot-dtc/swatch/commands/LoadLUT.hpp"
#include "ot-dtc/swatch/commands/SetThresholds.hpp"


SWATCH_REGISTER_CLASS(otdtc::swatch::Processor)

namespace otdtc {
namespace swatch {

using namespace ::swatch;

Processor::Processor(const ::swatch::core::AbstractStub& aStub) :
  serenity::swatch::DaughterCard(aStub)
{
  // 1) Declare commands
  registerCommand<commands::ConfigureComponentX>("configureComponentX");
  registerCommand<commands::LoadLUT>("loadLUT");
  registerCommand<commands::SetThresholds>("setThresholds");
  // FIXME: Rename the above commands as appropriate for your subsystem (and then add any others that you need)

  // 2) Declare FSMs
  // FIXME: Add the above commands to transitions of pre-defined FSMs where applicable.

  // 3) Declare class representing algorithm/payload firmware
  registerAlgo(new Algorithm());
}


Processor::~Processor()
{
}

} // namespace swatch
} // namespace otdtc
