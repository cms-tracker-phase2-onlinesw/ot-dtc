# Outer Tracker DTC plugin

This repository contains a SWATCH plugin library for the phase-2 Outer Tracker DTC. This plugin
builds on top of the Serenity SWATCH plugin, and in turn the EMP plugin, which define commands for
controlling and monitoring both the EMP firmware framework and all non-FPGA components on the
Serenity board. The OT DTC plugin adds configuration procedures and monitoring data for the OT
DTC-specific payload firmware.


## Dependencies

The plugin library has one direct dependency, the [Serenity SWATCH plugin](https://gitlab.cern.ch/p2-xware/software/serenity-herd/),
which itself builds on the following packages:
 * [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch)
 * [EMP software](http://serenity.web.cern.ch/serenity/emp-fwk/software/)
 * [EMP SWATCH plugin](https://gitlab.cern.ch/p2-xware/software/emp-herd/)
 * [SMASH](https://gitlab.cern.ch/p2-xware/software/smash)
 * [Serenity toolbox](https://gitlab.cern.ch/p2-xware/software/serenity-toolbox)


## Build

To build the plugin, firstly clone this repository:
```sh
git clone ssh://git@gitlab.cern.ch:7999/cms-cactus/phase2/software/plugins/examplesubsystem.git
```

The easiest way to build the plugin is inside a docker container, using the
[VSCode Dev Containers extension](https://code.visualstudio.com/docs/devcontainers/containers).
This repository includes a VSCode configuration file (`.devcontainer/devcontainer.json`) which
allows oneto build in a development container that already contains all dependencies, along with
relevant tooling like git. To use this setup you just need to open the path for the cloned
repository in VSCode, click on the green bar in the bottom left of the window and then click
"Reopen in container". Instructions for other development environments can be found
[here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html).

After VSCode has downloaded & started the development container (or other development environment set
up), to build the plugin simply follow the standard CMake build workflow in the VScode terminal:
```sh
mkdir build
cd build
cmake3 ..
make -j$(nproc)
```

## Deployment

This repository's CI pipeline automatically builds a container image containing this plugin along
with all dependencies, and uploads it to `gitlab-registry.cern.ch` using the following URL scheme:
 * Tags: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/ot-dtc/centos7/herd:vX.Y.Z`
 * Branches: `gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/ot-dtc/centos7/herd:BRANCHNAME-COMMITSHA`
   (where `COMMITSHA` is the first 8 characters of the git commit's SHA)

The simplest way to run the plugin on a Serenity is to run this image. Notably the image must
always be run using the `/opt/cactus/bin/serenity/docker-run.sh` script (which wraps `docker run`,
adding extra arguments to e.g. make device files accessible inside the container). E.g. for the
image from commit `1234abcd` on the `master` branch:
```sh
/opt/cactus/bin/serenity/docker-run.sh -d --name herd -p 3000:3000 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/ot-dtc/centos7/herd:master-1234abcd
``` 

